﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMQDemCLient
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();
            while (true)
            {
                Read(factory,connection,channel);
                Console.ReadLine();
            }
        }

        private static void Read(ConnectionFactory factory, IConnection connection, IModel channel)
        {
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);

                var data = JsonConvert.DeserializeObject<MessageObject>(message);
                Console.WriteLine(" [x] Received");
                Console.WriteLine($" [x] Message Id : {data.Id}" );
                Console.WriteLine($" [x] Message Body : {data.MessageString}" );
            };
            channel.BasicConsume(queue: "messages", noAck: false, consumer: consumer);
        }

        [Serializable]
        public class MessageObject
        {
            public string MessageString { get; set; }
            public Guid Id { get; set; }
        }
    }
}
