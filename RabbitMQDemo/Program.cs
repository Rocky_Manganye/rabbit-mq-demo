﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace RabbitMQDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();
            
                channel.QueueDeclare(queue: "messages",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

            while (true)
            {

                Console.WriteLine("Enter your message ..... ");
                var data = Console.ReadLine();
                if (!string.IsNullOrEmpty(data))
                {

                    var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new MessageObject
                    {
                        Id = Guid.NewGuid(),
                        MessageString = data
                    }));
                    channel.BasicPublish(exchange: "",
                                         routingKey: "messages",
                                         basicProperties: null,
                                         body: body);
                }
            }

        }
    }

    [Serializable]
    public class MessageObject
    {
        public string MessageString { get; set; }
        public Guid Id { get; set; }
    }
}
